document.getElementById('selectButton').addEventListener('click',function()
{
    removeFromScene('cockpit');
    removeFromScene('stationaryImages');
    removeFromScene('ecam');
    removeFromScene('pfd');
    removeFromScene('pfdFO');

    document.querySelector('a-scene').exitVR();
})
	  
function removeFromScene(id)
{		
	var scene = document.querySelector('a-scene').object3D;

    scene.children.forEach(child =>
    {
        if(child.el && child.el.id == id)
        {
            scene.remove(child);
            destroyMesh(child, scene);
        }
    });
}

function destroyMesh(mesh, scene) 
{

    if (mesh && mesh.geometry) 
    {
        mesh.geometry.dispose();
    }
    if (mesh.material) 
    {
        if (mesh.material.map) 
        {
            mesh.material.map.dispose();
        }
        mesh.material.dispose();
    }
		
    mesh.traverse( function ( child ) 
    {
        scene.remove(child);
        if(child.geometry)
        {
            child.geometry.dispose();
        }
        if(child.material)
        {
            if(child.material instanceof Array)
            {
                child.material.forEach(mat =>
                {
                    if(mat.map)
                    {
                        mat.map.dispose();
                    }
                    mat.dispose();
                })
            }
            else
            {
                if(child.material.map)
                {
                    child.material.map.dispose();
                }
                child.material.dispose();
            }
        }
    });
}