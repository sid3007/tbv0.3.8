// window.onload = function()
// document.addEventListener('click', function()
document.getElementById('pdfButton').addEventListener('click', function()
{
    var url = 'https://cdn.glitch.com/d908b2ef-c59e-459d-85a9-b77a1cde405d%2Fmaxfield_s.pdf?1541229650737';
        pdfjsLib.GlobalWorkerOptions.workerSrc = "js/libs/pdf.worker.js";
    var count = 1;
    getPdf(1);
    document.getElementById("nextButton").addEventListener("click", function () 
    {
        count += 1;
        getPdf(count);
    })
    document.getElementById("prevButton").addEventListener("click", function () 
    {
        count -= 1;
        getPdf(count);
    });

    function getPdf(pageNum) 
    {
        pdfjsLib.getDocument(url).then(function getPdfHelloWorld(pdf) 
        {
            function getPage(pageNum) 
            {
                pdf.getPage(pageNum).then(function getPageHelloWorld(page) 
                {
                    var scale = 4;
                    var viewport = page.getViewport(scale);
                    var canvas = document.getElementById('my-canvas');
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
                    var renderContext = 
                    {
                        canvasContext: context,
                        viewport: viewport
                    };

                    page.render(renderContext);
                });
            }
            getPage(pageNum);
        });
    }
});