AFRAME.registerComponent('clip',{
  
  schema: {
    constant:{ default: 0 } ,
    target: {
      type: "selectorAll"
    },
  },
  
  init: function() {
    var obj = this.el.getObject3D('mesh');
    var scene = document.querySelector('a-scene').object3D
    var mat = obj.material;
    this.vecdir = new THREE.Vector3();
    this.vecpos = new THREE.Vector3();
    var planes = []
    for(var i = 0; i < this.data.target.length; i++ ) {
        planes.push(new THREE.Plane(new THREE.Vector3(), 0));
        var helper = new THREE.PlaneHelper( planes[i], 2, 0xffff00 );
        // scene.add(helper);
    }
    mat.clippingPlanes = planes;
  }, 
                         
  update: function() { 
    var mat = this.el.getObject3D('mesh').material;
    for(var i = 0; i < this.data.target.length; i++) {
      this.data.target[i].object3D.getWorldDirection(this.vecdir);
      this.data.target[i].object3D.getWorldPosition(this.vecpos);
      mat.clippingPlanes[i].setFromNormalAndCoplanarPoint(
        this.vecdir,
        this.vecpos);
    }
  },
  
  tick: function() {
    var mat = this.el.getObject3D('mesh').material;
    for(var i = 0; i < this.data.target.length; i++) {
      this.data.target[i].object3D.getWorldDirection(this.vecdir);
      this.data.target[i].object3D.getWorldPosition(this.vecpos);
      mat.clippingPlanes[i].setFromNormalAndCoplanarPoint(
        this.vecdir,
        this.vecpos);
    }
  }
  
  
})